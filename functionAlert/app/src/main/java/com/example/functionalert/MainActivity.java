package com.example.functionalert;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btn;
    ImageView imgv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        thongbao("Tien Loi");
//        int n = tinhtong();
        btn = findViewById(R.id.btnChangeImg);
        imgv = findViewById(R.id.imgView);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgv.setImageResource(R.drawable.ios_icon);
            }
        });
    }
    public void thongbao( String s) {
        Toast.makeText(
                MainActivity.this,
                s,
                Toast.LENGTH_SHORT
        ).show();
    }
    public int tinhtong() {
        return 10;
    }
}